﻿using ChangeHomeNetWork.Controllers;
using ChangeHomeNetWork.Services;
using Microsoft.AspNetCore.Hosting;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace SchedulerUploadFile
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();

            // Start lịch
            await scheduler.Start();

            /////////////////// Job chạy hàng ngày
            IJobDetail job = JobBuilder.Create<RunJob>().WithIdentity("JobUpload", "GroupUpload").Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("Trigger", "GroupUpload")
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(86400).RepeatForever())
                //.WithSimpleSchedule(x => x.WithIntervalInSeconds(60).RepeatForever())
                .Build();
            await scheduler.ScheduleJob(job, trigger);
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(86400));
            }
            await scheduler.Shutdown();
            Console.WriteLine("Close application");
        }
    }
    public class RunJob : IJob
    {
        private readonly PhoneNumberServices _phoneNumberServices;
        public async Task Execute(IJobExecutionContext context)
        {
            // Gọi đến hàm đẩy file lên ftp
            await Console.Out.WriteLineAsync("Run job: (RunJob) success!");
            var control = new PhoneNumberController(_phoneNumberServices);
            var urlPath = "D:/Doc_Haitq/Code_Me/vnp_test/ChangeHomeNetWork/ChangeHomeNetWork/wwwroot/FileUpload/NewFile.csv";
            control.FtpCreateFolderForScheduler(urlPath);
        }
    }
}
