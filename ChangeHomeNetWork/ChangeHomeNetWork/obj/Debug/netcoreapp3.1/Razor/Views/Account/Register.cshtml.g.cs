#pragma checksum "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "932cb5ce9e7a922724f2c73a594c05e6e1fc905c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_Register), @"mvc.1.0.view", @"/Views/Account/Register.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\_ViewImports.cshtml"
using ChangeHomeNetWork;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\_ViewImports.cshtml"
using ChangeHomeNetWork.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"932cb5ce9e7a922724f2c73a594c05e6e1fc905c", @"/Views/Account/Register.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1eb179a91988a99b1c793146442c5afc7f7e304c", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_Register : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<RegisterViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
  
    ViewBag.Title = "Đăng ký tài khoản";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<section class=\"content-header\">\r\n    <h2>");
#nullable restore
#line 7 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
   Write(ViewBag.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n</section>\r\n<section class=\"content\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            <div class=\"box box-primary\">\r\n                <div class=\"box-body with-border\">\r\n");
#nullable restore
#line 14 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                     using (Html.BeginForm("Register", "Account", FormMethod.Post, new { role = "form" }))
                    {
                        

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                   Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                   Write(Html.ValidationSummary("", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div class=\"form-group\">\r\n                            <label>Email</label>\r\n                            ");
#nullable restore
#line 20 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                       Write(Html.TextBoxFor(m => m.Email, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label>Mật khẩu</label>\r\n                            ");
#nullable restore
#line 24 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                       Write(Html.PasswordFor(m => m.Password, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label>Nhập lại mật khẩu</label>\r\n                            ");
#nullable restore
#line 28 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                       Write(Html.PasswordFor(m => m.ConfirmPassword, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n");
            WriteLiteral(@"                        </div>
                        <div class=""box-footer"">
                            <div class=""pull-right"">
                                <button class=""btn btn-primary"" type=""submit"">Đăng ký</button>
                            </div>
                        </div>
");
#nullable restore
#line 38 "D:\Doc_Haitq\Code_Me\vnp_test\ChangeHomeNetWork\ChangeHomeNetWork\Views\Account\Register.cshtml"
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<RegisterViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
