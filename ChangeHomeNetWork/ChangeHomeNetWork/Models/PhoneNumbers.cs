﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChangeHomeNetWork.Models
{
    public class PhoneNumbers
    {
        [Key]
        public string Id { get; set; }
        public string STT { get; set; }
        public string PhoneNumber { get; set; }
        public string HomeNetworkOld { get; set; }
        public string HomeNetworkNew { get; set; }
        public DateTime DateChange { get; set; }
    }
}
