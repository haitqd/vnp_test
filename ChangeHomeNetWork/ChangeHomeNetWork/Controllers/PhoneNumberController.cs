﻿using ChangeHomeNetWork.Log;
using ChangeHomeNetWork.Models;
using ChangeHomeNetWork.Services;
using CsvHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static ChangeHomeNetWork.Log.Logger;

namespace ChangeHomeNetWork.Controllers
{
    public class PhoneNumberController : Controller
    {
        private readonly PhoneNumberServices _phoneNumberServices;
        private static readonly string ftpAddress = "ftp://127.0.0.1";
        private static readonly string username = "haitq";
        private static readonly string password = "1234567";
        public PhoneNumberController(PhoneNumberServices phoneNumberServices)
        {
            _phoneNumberServices = phoneNumberServices;
        }
        [HttpGet]
        public IActionResult Index(List<PhoneNumbers> phoneNumbers)
        {
            try
            {
                phoneNumbers = phoneNumbers != null ? phoneNumbers : new List<PhoneNumbers>();
                Logger.WriteLog(LogType.Info, "Danh sách SĐT:" + JsonSerializer.Serialize(phoneNumbers));
                return View(phoneNumbers);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return View(new List<PhoneNumbers>());
            }

        }
        [HttpPost]
        public IActionResult GetList(string searchPhone)
        {
            try
            {
                Logger.WriteLog(LogType.Info, "Start Tìm kiếm theo SĐT:" + searchPhone);
                var res = _phoneNumberServices.GetPhoneNumbersAsync(searchPhone);
                Logger.WriteLog(LogType.Info, "End Tìm kiếm theo SĐT:" + searchPhone + ": " + JsonSerializer.Serialize(res));
                return View("Index", res);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return View("Index", new List<PhoneNumbers>());
            }
        }

        public bool FtpCreateFolderForScheduler(string dirPath)
        {
            //var path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\FileUpload"}" + "\\" + "NewFile.csv";
            var path = dirPath;
            if (path != null)
            {
                var checkSuccess = ftpTransfer(path);
                if (checkSuccess)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public IActionResult FtpCreateFolder()
        {
            var path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\FileUpload"}" + "\\" + "NewFile.csv";
            if (path != null)
            {
                var checkSuccess = ftpTransfer(path);
                if (checkSuccess)
                {
                    return View("Index", new List<PhoneNumbers>());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return NotFound();
            }
        }
        public bool ftpTransfer(string fileName)
        {
            try
            {
                try
                {
                    var dateNow = DateTime.Now.ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("HHmmss");
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress + "/var/data");
                    request.Credentials = new NetworkCredential(username, password);
                    request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    // Nếu có Dir trên ftp rồi thì không tạo
                    if (response.StatusCode == FtpStatusCode.OpeningData)
                    {
                        // Tạo Dỉ theo ngày để gán file vào
                        FtpWebRequest requestByDay = (FtpWebRequest)WebRequest.Create(ftpAddress + "/var/data/" + dateNow);
                        requestByDay.Credentials = new NetworkCredential(username, password);
                        requestByDay.Method = WebRequestMethods.Ftp.MakeDirectory;
                        FtpWebResponse responseByDay = (FtpWebResponse)requestByDay.GetResponse();
                        if (responseByDay.StatusCode == FtpStatusCode.PathnameCreated)
                        {
                            var uri = responseByDay.ResponseUri;
                            string PureFileName = new FileInfo(fileName).Name;// + dateNow;
                            String uploadUrl = String.Format("{0}/{1}/{2}", uri.Scheme + "://" + uri.Host, uri.LocalPath, PureFileName);
                            FtpWebRequest requestUpload = (FtpWebRequest)WebRequest.Create(uploadUrl);
                            requestUpload.Credentials = new NetworkCredential(username, password);
                            requestUpload.Method = WebRequestMethods.Ftp.UploadFile;

                            StreamReader sourceStream = new StreamReader(fileName);
                            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                            sourceStream.Close();
                            requestUpload.ContentLength = fileContents.Length;
                            Stream requestStream = requestUpload.GetRequestStream();
                            requestStream.Write(fileContents, 0, fileContents.Length);
                            requestStream.Close();
                            FtpWebResponse responseUpload = (FtpWebResponse)request.GetResponse();
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    // Chưa có Dir trên ftp thì tạo Dir
                    // Tạo dir trên ftp
                    var dateNow = DateTime.Now.ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("HHmmss");
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress + "/var/data");
                    request.Credentials = new NetworkCredential(username, password);
                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    if (response.StatusCode == FtpStatusCode.PathnameCreated)
                    {
                        // Tạo Dỉ theo ngày để gán file vào
                        FtpWebRequest requestByDay = (FtpWebRequest)WebRequest.Create(ftpAddress + "/var/data/" + dateNow);
                        requestByDay.Credentials = new NetworkCredential(username, password);
                        requestByDay.Method = WebRequestMethods.Ftp.MakeDirectory;
                        FtpWebResponse responseByDay = (FtpWebResponse)requestByDay.GetResponse();
                        if (responseByDay.StatusCode == FtpStatusCode.PathnameCreated)
                        {
                            var uri = responseByDay.ResponseUri;
                            string PureFileName = new FileInfo(fileName).Name;// + dateNow;
                            String uploadUrl = String.Format("{0}/{1}/{2}", uri.Scheme + "://" + uri.Host, uri.LocalPath, PureFileName);
                            FtpWebRequest requestUpload = (FtpWebRequest)WebRequest.Create(uploadUrl);
                            requestUpload.Credentials = new NetworkCredential(username, password);
                            requestUpload.Method = WebRequestMethods.Ftp.UploadFile;

                            StreamReader sourceStream = new StreamReader(fileName);
                            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                            sourceStream.Close();
                            requestUpload.ContentLength = fileContents.Length;
                            Stream requestStream = requestUpload.GetRequestStream();
                            requestStream.Write(fileContents, 0, fileContents.Length);
                            requestStream.Close();
                            FtpWebResponse responseUpload = (FtpWebResponse)request.GetResponse();
                        }
                    }
                    Console.WriteLine(ex.ToString());
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return false;
            }
        }
        [HttpPost]
        public IActionResult Index(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            try
            {
                if (file != null)
                {
                    Logger.WriteLog(LogType.Info, "Start ghi file:" + file?.FileName);
                    string fileName = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
                    Logger.WriteLog(LogType.Info, "End ghi file:" + fileName);
                    using (FileStream fileStream = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                    var phoneNumber = GetPhoneNumbers(file.FileName);
                    return Index(phoneNumber);
                }
                else
                {
                    Logger.WriteLog(LogType.Error, "Không có file:" + file);
                    return Index(new List<PhoneNumbers>());
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return Index(new List<PhoneNumbers>());
            }
        }
        private List<PhoneNumbers> GetPhoneNumbers(string fileName)
        {
            try
            {
                List<PhoneNumbers> phoneNumbersSave = new List<PhoneNumbers>();
                List<PhoneNumbers> phoneNumbers = new List<PhoneNumbers>();
                // Đọc file csv
                Logger.WriteLog(LogType.Info, "Start đọc file: " + fileName);
                var path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\files"}" + "\\" + fileName;
                Logger.WriteLog(LogType.Info, "End đọc file từ path: " + path);
                if (path.EndsWith(".csv"))
                {
                    using (var reader = new StreamReader(path))
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        if (csv.Read() == true)
                        {
                            csv.Read();
                            csv.ReadHeader();
                            while (csv.Read())
                            {
                                var objPhone = new PhoneNumbers();
                                objPhone.Id = Guid.NewGuid().ToString();
                                objPhone.STT = csv.GetField(0);
                                objPhone.PhoneNumber = csv.GetField(1);
                                objPhone.HomeNetworkOld = csv.GetField(2);
                                objPhone.HomeNetworkNew = csv.GetField(3);
                                objPhone.DateChange = Convert.ToDateTime(csv.GetField(4));
                                //var phoneNumber = csv.GetRecord<PhoneNumbers>();
                                if (!string.IsNullOrWhiteSpace(objPhone.HomeNetworkNew))
                                    phoneNumbers.Add(objPhone);
                                phoneNumbersSave.Add(objPhone);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Định dạng file không đúng.");
                        }
                    }
                    Logger.WriteLog(LogType.Info, "Đọc dữ liệu trong file:" + JsonSerializer.Serialize(phoneNumbers));
                }
                else
                {
                    Logger.WriteLog(LogType.Info, "File không đúng định dạng: " + path);
                }
                // Tạo file csv
                path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\FileUpload"}";
                using (var write = new StreamWriter(path + "\\NewFile.csv"))
                using (var csv = new CsvWriter(write, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(phoneNumbers);
                }
                // Gửi file lên FTP
                ftpTransfer(path + "\\NewFile.csv");
                var existData = true;
                if (phoneNumbersSave != null && phoneNumbersSave.Count() > 0)
                {
                    existData = SaveData(phoneNumbersSave);
                }
                if (existData)
                {
                    return phoneNumbersSave;
                }
                else
                {
                    return new List<PhoneNumbers>();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return new List<PhoneNumbers>();
            }

        }
        public bool SaveData(List<PhoneNumbers> phoneNumbers)
        {
            try
            {
                if (phoneNumbers != null)
                {
                    Logger.WriteLog(LogType.Info, "Start lưu dữ liệu: " + JsonSerializer.Serialize(phoneNumbers));
                    var res = _phoneNumberServices.CreatePhoneNumberAsync(phoneNumbers);
                    Logger.WriteLog(LogType.Info, "End lưu thành công" + res.ToString());
                    return true;
                }
                else
                {
                    WriteLog(LogType.Error, "Start lưu dữ liệu: " + JsonSerializer.Serialize(phoneNumbers));
                    WriteLog(LogType.Error, "End lưu thất bại" + false.ToString());
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                return false;
            }
        }
    }
}
