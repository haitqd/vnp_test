﻿using ChangeHomeNetWork.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChangeHomeNetWork.Controllers
{
    public class AccountController : Controller
    {
        private RoleManager<IdentityRole> _roleManager { get; }
        private UserManager<IdentityUser> _userManager { get; }
        private SignInManager<IdentityUser> _signInManager { get; }
        public AccountController(
          UserManager<IdentityUser> userManager,
          SignInManager<IdentityUser> signInManager,
          RoleManager<IdentityRole> roleManager
          )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }
        private ActionResult RedirectToLocal()
        {
            return RedirectToAction("Index", "PhoneNumber");
        }
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(model.UserName);
                    await _signInManager.SignInAsync(user, true);
                    return RedirectToLocal();
                }
                else if (result.IsLockedOut)
                {
                    return View("Login");
                }
                else if (result.RequiresTwoFactor)
                {
                    return RedirectToAction("Login", new { ReturnUrl = returnUrl, RememberMe = false });
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View("Login");
                }
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = new IdentityUser { UserName = model.Email, Email = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var identityRole = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Admin", NormalizedName = "ADMIN" };
                await _roleManager.CreateAsync(identityRole);
                await _userManager.AddToRoleAsync(user, identityRole.Name);
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ModelState.AddModelError("", result.Errors.FirstOrDefault().Description);
                return View();
            }
        }
    }
}
