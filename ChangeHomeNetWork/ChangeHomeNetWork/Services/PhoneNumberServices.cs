﻿using ChangeHomeNetWork.Data;
using ChangeHomeNetWork.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChangeHomeNetWork.Services
{
    public class PhoneNumberServices
    {
        private readonly DataContext _dataContext;
        public PhoneNumberServices(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public bool CreatePhoneNumberAsync(List<PhoneNumbers> phoneNumber)
        {
            var lstPhoneNum = new List<PhoneNumbers>();
            foreach (var item in phoneNumber)
            {
                var objPhone = getPhoneNumberByPhoneAsync(item.PhoneNumber);
                if (objPhone != null)
                {
                    if (item.PhoneNumber != objPhone.PhoneNumber)
                    {
                        item.PhoneNumber.Trim();
                        lstPhoneNum.Add(item);
                    }
                }
                else
                {
                    item.PhoneNumber.Trim();
                    lstPhoneNum.Add(item);
                }
            }
            _dataContext.PhoneNumbers.AddRangeAsync(lstPhoneNum);
            var created = _dataContext.SaveChangesAsync();
            return true;
        }
        public PhoneNumbers getPhoneNumberByPhoneAsync(string phoneNum)
        {
            return _dataContext.PhoneNumbers.ToList().FirstOrDefault(x => x.PhoneNumber.Contains(phoneNum));
        }
        public List<PhoneNumbers> GetPhoneNumbersAsync(string phonenum)
        {
            return _dataContext.PhoneNumbers.Where(x => string.IsNullOrWhiteSpace(phonenum) || x.PhoneNumber.Contains(phonenum.Trim())).OrderBy(x => x.STT).ToList();
        }
    }
}
